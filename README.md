# BuyPassport - Купить сканы паспортов граждан РФ

Продаётся база паспортов граждан РФ и зарубежъя! В базе сканы основной страницы и прописки. Также имеются полные сканы паспортов и сканы с другими документами.

# Цены

* 1 шт. 5$/шт.
* от 10 шт. 4$/шт.
* от 20 шт. 3.5$/шт.
* от 100 шт. 2.50$/шт.
* от 1000 шт. 1.75$/шт.

# Оплата
* Bitcoin
* Dash

# Контакты
***whirlpoolslat@protonmail.ch***

# Теги & Ключевые слова
Продажа паспортных данных в России. Купить паспортные данный граждан РФ и зарубежъя. Продажа сканов паспортов и документов. Продажа базы паспортных данных.
